from PyPDF2 import PdfFileWriter, PdfFileReader
import io
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
import csv

def main():
    output = PdfFileWriter()
    with open('tuomarihaku.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        for row in csv_reader:
            try:
                kentta = str(int(row[0]))
            except ValueError:
                kentta = str(0)
            except:
                return
            aika = row[1]
            packet = io.BytesIO()
            # create a new PDF with Reportlab
            can = canvas.Canvas(packet, pagesize=letter)
            can.setFont("Helvetica",20)
            can.drawString(330, 640, kentta)
            can.drawString(330, 598, aika)
            can.save()

            #move to the beginning of the StringIO buffer
            packet.seek(0)
            new_pdf = PdfFileReader(packet)
            # read your existing PDF
            existing_pdf = PdfFileReader(open("core.pdf", "rb"))

            # add the "watermark" (which is the new pdf) on the existing page
            page = existing_pdf.getPage(0)
            page.mergePage(new_pdf.getPage(0))
            output.addPage(page)


    # finally, write "output" to a real file
    outputStream = open("hakemislaput.pdf", "wb")
    output.write(outputStream)
    outputStream.close()

main()